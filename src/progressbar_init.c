//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libProgressBar
// C library to display a progress bar to show a user how far along he is in a process.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILE
//------------------------------------------------------------------------------
#include "progressbar.h"
#include "progressbar_data.h"
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
int vprogressbar_nbLINES 							= 0;
int vprogressbar_nbCOLS 							= 0;
int vprogressbar_hundredpercent						= 0;
int vprogressbar_x									= 0;
int vprogressbar_lines								= 0;
pthread_t vprogressbar_thread						= 0;
pthread_mutex_t vprogressbar_mutex					= PTHREAD_MUTEX_INITIALIZER;
enum eprogressbar_style vprogressbar_style			= UNDEFINED;
enum eprogressbar_location vprogressbar_location	= BOTTOM;
enum eprogressbar_color vprogressbar_color			= WHITE;
enum eprogressbar_type vprogressbar_type			= DETERMINED;
//------------------------------------------------------------------------------
// INTERNAL ROUTINES
//------------------------------------------------------------------------------
static void progressbar_getmaxyx( int *lines, int *cols ) {
	struct winsize w;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	*lines = w.ws_row;
	*cols = w.ws_col;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void progressbar_setDimensions( void ) {
	switch (vprogressbar_style) {										// Additional information
	case FRAMED:
		vprogressbar_lines = 3;											// Progress bar number of lines
		switch (vprogressbar_type) {
		case DETERMINED:
			vprogressbar_hundredpercent = vprogressbar_nbCOLS - 7;		// Compute how much 100% represents
			break;
		case INDETERMINED:
			vprogressbar_hundredpercent = vprogressbar_nbCOLS - 2;		// Compute how much 100% represents
			break;
		}
		break;
	case SIMPLE:
		vprogressbar_lines = 1;											// Progress bar number of lines
		switch (vprogressbar_type) {
		case DETERMINED:
			vprogressbar_hundredpercent = vprogressbar_nbCOLS - 5;		// Compute how much 100% represents
			break;
		case INDETERMINED:
			vprogressbar_hundredpercent = vprogressbar_nbCOLS;			// Compute how much 100% represents
			break;
		}
		break;
	case TEXT:
		vprogressbar_lines = 1;											// Progress bar number of lines
		switch (vprogressbar_type) {
		case DETERMINED:
			vprogressbar_hundredpercent = vprogressbar_nbCOLS - 7;		// Compute how much 100% represents
			break;
		case INDETERMINED:
			vprogressbar_hundredpercent = vprogressbar_nbCOLS - 2;		// Compute how much 100% represents
			break;
		}
		break;
	default:
		break;
	}
	switch (vprogressbar_location) {
	case TOP:
		vprogressbar_x = 1;												// Set top line of the progress bar
		break;
	default:
	case BOTTOM:
		vprogressbar_x = vprogressbar_nbLINES - vprogressbar_lines + 1;	// Set top line of the progress bar
		break;
	}
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void progressbar_sighandler(int sig) {
	progressbar_getmaxyx(&vprogressbar_nbLINES, &vprogressbar_nbCOLS);	// Number of lines and cols of the screen
	progressbar_setDimensions();										// Set dimensions depending on terminal size
	progressbar_decoration(0);											// Draw progress bar
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void progressbar_error(const char *format, ...) {
	va_list argp;
	char buff[512];
	va_start(argp, format);
	vsprintf(buff, format, argp);
	va_end(argp);
	fprintf(stderr, "\n%s%s %s%s\n\n", cprogressbar_FOREGROUND_RED, cprogressbar_ERRORPREFIX, buff, cprogressbar_STOP_COLOR);
	exit(EXIT_FAILURE);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void progressbar_getcursor( int *line, int *col ) {
	char buf[16];
	struct termios save, raw;

	tcgetattr(0, &save);
	cfmakeraw(&raw);
	tcsetattr(0, TCSANOW, &raw);
	REQUEST_CURSOR_POSITION;
	ssize_t n = read(0, buf, sizeof(buf));
	sscanf(buf + 2, "%d;%dR", line, col);
	tcsetattr(0, TCSANOW, &save);
}
//------------------------------------------------------------------------------
// MAIN ROUTINE
//------------------------------------------------------------------------------
void progressbar_init( enum eprogressbar_type type,
                       enum eprogressbar_style style,
                       enum eprogressbar_location location) {
	CHECK_IF_PROGRESSBAR_IS_NOT_DEFINED;
	CHECK_IF_STDINOUT_REFER_TO_A_TERMINAL;
	vprogressbar_type = type;							// Determinated or indeterminated progress (task running length)
	vprogressbar_style = style;							// Progress bar style
	vprogressbar_location = location;					// Progress bar location
	signal(SIGWINCH, progressbar_sighandler);
	progressbar_sighandler(SIGWINCH);
}
//------------------------------------------------------------------------------
